package net.sarazan.plugin.host

import android.support.annotation.StringRes
import net.sarazan.plugin.alerts.PluginAlert
import net.sarazan.plugin.internal.HostComponent

interface HostAlerts : HostComponent {
    fun show(alert: PluginAlert)
    fun toast(text: String)
    fun toast(@StringRes text: Int)
}