package net.sarazan.plugin

import net.sarazan.plugin.alerts.PluginAlerts
import net.sarazan.plugin.cards.PluginCards
import net.sarazan.plugin.handlers.PluginHandlers
import net.sarazan.plugin.host.PluginHostInterface
import net.sarazan.plugin.internal.PluginBuilder

class Plugin private constructor(
        val id: String = "",
        val cards: PluginCards?,
        val alerts: PluginAlerts?,
        val handlers: PluginHandlers?
) {

    lateinit var host: PluginHostInterface
        private set

    fun initialize(host: PluginHostInterface) {
        this.host = host
        cards?.initialize(this)
        alerts?.initialize(this)
    }

    @PluginBuilder
    class Builder(val id: String) {

        var cards: PluginCards? = null
        var alerts: PluginAlerts? = null
        var handlers: PluginHandlers? = null

        fun setCard(cards: PluginCards?) = apply {
            this.cards = cards
        }

        fun setAlerts(alerts: PluginAlerts?) = apply {
            this.alerts = alerts
        }

        fun setHandlers(handlers: PluginHandlers?) = apply {
            this.handlers = handlers
        }

        fun build(): Plugin {
            return Plugin(id, cards, alerts, handlers)
        }
    }
}