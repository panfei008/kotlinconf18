package net.sarazan.host

import net.sarazan.plugin.handlers.HostHandlers
import net.sarazan.plugin.host.HostAlerts

class StandardHostInterface : HostInterface() {
    override val alerts: HostAlerts by lazy { StandardHostAlerts(context) }
    override val handlers: HostHandlers by lazy { StandardHostHandlers(context) }
}