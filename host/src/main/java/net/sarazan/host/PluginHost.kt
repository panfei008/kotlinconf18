package net.sarazan.host

import android.content.Context
import net.sarazan.plugin.Plugin
import net.sarazan.plugin.host.PluginHostInterface

class PluginHost(
        val c: Context,
        val plugins: List<Plugin>)
{
    fun getCardAdapter(): CardAdapter {
        return CardAdapter(this)
    }

    class Builder(val c: Context) {

        val plugins = mutableListOf<Plugin>()

        fun addPlugin(plugin: Plugin) = apply {
            plugins.add(plugin)
        }

        fun build(host: Class<out HostInterface>): PluginHost {
            val plugins = plugins.toList()
            val hosts = plugins.associate {
                Pair(it, host.newInstance().apply { initialize(c, it, plugins) })
            }
            return PluginHost(c, plugins).apply {
                plugins.forEach { it.initialize(hosts[it]!!) }
            }
        }
    }
}